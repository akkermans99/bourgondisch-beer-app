import { getTestBed, TestBed } from '@angular/core/testing'

import { CommentService } from './comment.service'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

describe('CommentService', () => {
  let service: CommentService
  let injector: TestBed
  let httpMock: HttpTestingController
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CommentService]
    })
    injector = getTestBed()
    service = injector.get(CommentService)
    httpMock = injector.get(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
