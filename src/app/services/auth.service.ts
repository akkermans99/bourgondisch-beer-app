import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { User, UserRoles } from '../domain/user'
import { Router } from '@angular/router'
import { tap } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { CartService } from './cart.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedInUser = new BehaviorSubject<boolean>(false)
  public loggedInUserName = new BehaviorSubject<string>('')
  private isAdminUser = new BehaviorSubject<boolean>(false)
  private isPlainUser = new BehaviorSubject<boolean>(false)
  private apiUrl = 'https://avans-beer-api.herokuapp.com/api'
  private readonly currentUser = 'currentuser'
  private readonly currentToken = 'token'
  public isLoggedIn = false

  // store the URL so we can redirect after logging in
  public readonly redirectUrl: string = '/dashboard'

  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' })

  /**
   *
   * @param router: Router
   * @param http: HttpClient
   */
  constructor(private router: Router, private http: HttpClient, private cartService: CartService) {
    this.getCurrentUser().subscribe({
      next: (user: User) => {
        console.log(`${user.emailAddress} logged in`)
        this.isLoggedInUser.next(true)
        this.loggedInUserName.next(user.username)
        // Verify the possible roles the user can have.
        // Add more verifications when nessecary.
        /*user.hasRole(UserRole.Admin).subscribe(result => this.isAdminUser.next(result))
        user.hasRole(UserRole.Basic).subscribe(result => this.isPlainUser.next(result))*/
        this.isAdminUser.next(user.hasRole(UserRoles.Admin))
        this.isPlainUser.next(user.hasRole(UserRoles.Basic))
      },
      error: message => {
        this.isLoggedInUser.next(false)
        this.isAdminUser.next(false)
        this.isPlainUser.next(false)
        this.router.navigate(['/login'])
      }
    })
  }

  /**
   * Log in
   *
   * @param userData: any
   */
  login(userData) {
    console.log('login')
    console.log(`POST ${this.apiUrl}/users/login`)
    this.isLoggedIn = false
    return this.http
      .post(`${this.apiUrl}/users/login`, JSON.stringify(userData), { headers: this.headers })
      .pipe(
        tap(
          data => console.log(data),
          error => {
            console.error(error)
            this.isLoggedIn = false
          }
        )
      )
      .subscribe({
        next: (response: any) => {
          const currentUser = new User(response.user)
          console.dir(currentUser)
          this.saveCurrentUser(currentUser, response.token)
          // Notify all listeners that we're logged in.
          this.isLoggedInUser.next(true)
          this.loggedInUserName.next(currentUser.username)
          /*currentUser.hasRole(UserRole.Admin).subscribe(result => this.isAdminUser.next(result))
          currentUser.hasRole(UserRole.Basic).subscribe(result => this.isPlainUser.next(result))*/
          this.isAdminUser.next(currentUser.hasRole(UserRoles.Admin))
          this.isPlainUser.next(currentUser.hasRole(UserRoles.Basic))
          // If redirectUrl exists, go there
          // this.router.navigate([this.redirectUrl]);
          this.isLoggedIn = true
        },
        error: (message: any) => {
          console.log('error:', message)
        }
      })
  }

  /**
   * Log out.
   */
  logout() {
    console.log('logout')
    localStorage.removeItem(this.currentUser)
    localStorage.removeItem(this.currentToken)
    this.cartService.clearCart()
    this.isLoggedInUser.next(false)
    this.router.navigate(['/dashboard'])
    this.isAdminUser.next(false)
  }

  /**
   * Get the currently logged in user.
   */
  public getCurrentUser(): Observable<User> {
    return new Observable(observer => {
      const localUser: any = JSON.parse(localStorage.getItem(this.currentUser))
      if (localUser) {
        console.log('localUser found')
        observer.next(new User(localUser))
        observer.complete()
      } else {
        console.log('NO localUser found')
        observer.error('NO localUser found')
        observer.complete()
      }
    })
  }

  /**
   *
   */
  private saveCurrentUser(user: User, token: string): void {
    localStorage.setItem(this.currentUser, JSON.stringify(user))
    localStorage.setItem(this.currentToken, token)
  }

  get userFullName(): Observable<string> {
    console.log('userFullName ' + this.loggedInUserName.value)
    return this.loggedInUserName.asObservable()
  }

  /**
   *
   */
  get userIsLoggedIn(): Observable<boolean> {
    console.log('userIsLoggedIn() ' + this.isLoggedInUser.value)
    return this.isLoggedInUser.asObservable()
  }

  /**
   *
   */
  get userIsAdmin(): Observable<boolean> {
    console.log('userIsAdmin() ' + this.isAdminUser.value)
    return this.isAdminUser.asObservable()
  }

  /**
   *
   */
  get userIsPlain(): Observable<boolean> {
    console.log('userIsPlain() ' + this.isPlainUser.value)
    return this.isPlainUser.asObservable()
  }
}
