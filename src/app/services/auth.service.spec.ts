import { getTestBed, TestBed } from '@angular/core/testing'

import { AuthService } from './auth.service'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { CartService } from './cart.service'
import { User, UserRoles } from '../domain/user'

describe('AuthService', () => {
  const testUser: User = {
    _id: '5de682f4fb292e00246bd285',
    username: 'lgaakker',
    password: '$2b$10$4iA9152WaNwM/esqONt3GuD0gSlF5.ow3Lk.Sr.q61.HWDMigYHVi',
    emailAddress: 'lars@akkermansjes.nl',
    birthDate: new Date(1999, 8, 29),
    postalCode: '4901 ZG',
    address: 'Muldersteeg 10',
    country: 'Nederland',
    roles: [0, 1],
    orders: [],
    hasRole(role: UserRoles): boolean {
      return true
    }
  }
  const testToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
    'eyJ1c2VyIjp7InVzZXJuYW1lIjoibGdhYWtrZXIiLCJpZCI6IjVkZTY4MmY0ZmIyOTJlMDAyNDZiZDI4NSJ9LCJpYXQiOjE1ODU4NDc5MzksImV4cCI6MTU4NTkzNDMzOX0.' +
    'FHenP_bQ8a0Fyr5zmIGPkxAL0h6hWzITF2LaFKXg4F4'

  let injector: TestBed
  let service: AuthService
  let cartService: CartService
  let httpMock: HttpTestingController
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [AuthService, CartService]
    })
    injector = getTestBed()
    service = injector.get(AuthService)
    httpMock = injector.get(HttpTestingController)
    cartService = injector.get(CartService)
    localStorage.setItem('currentuser', JSON.stringify(testUser))
    localStorage.setItem('token', testToken)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
