import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private apiUrl = 'https://avans-beer-api.herokuapp.com/api/comments'
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  public editCommentById(id: string, commentData: any): Observable<any> {
    const url = `${this.apiUrl}/${id}`
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    }
    return this.http
      .put(url, JSON.stringify(commentData), this.httpOptions)
      .pipe(tap(data => console.log(data), error => console.log(error)))
  }
}
