import { Injectable } from '@angular/core'
import { Beer } from '../domain/beer'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'
import { error } from 'util'

@Injectable({
  providedIn: 'root'
})
export class BeerService {
  private beersUrl = 'https://avans-beer-api.herokuapp.com/api/beers'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  getBeers(): Observable<any> {
    console.log(this.beersUrl)
    return this.http.get<any>(this.beersUrl).pipe(
      tap(_ => console.log('fetched beers')),
      catchError(this.handleError<any>('getBeers', []))
    )
  }

  getBeerById(id: string): Observable<any> {
    const url = `${this.beersUrl}/${id}`
    console.log(url)
    return this.http.get<any>(url).pipe(
      tap(_ => console.log('fetched beer with id ' + id)),
      catchError(this.handleError<any>('getBeer'))
    )
  }

  getBeersBySpecie(specie: string): Observable<any> {
    const url = `${this.beersUrl}/species/${specie}`
    console.log(url)
    return this.http.get<any>(url).pipe(
      tap(_ => console.log(`fetched beer with specie: ${specie}`)),
      catchError(this.handleError<any>('getBeersBySpecie'))
    )
  }

  getBeerByName(name: string): Observable<any> {
    const url = `${this.beersUrl}/name/${name}`
    console.log(url)
    return this.http.get<any>(url).pipe(
      tap(_ => console.log('fetched beer by name')),
      catchError(this.handleError<any>('getBeerByName'))
    )
  }

  getDashboardBeers(): Observable<any> {
    const url = `${this.beersUrl}/home/dashboard`
    return this.http.get<any>(url).pipe(
      tap(_ => console.log('fetched dashboard beers')),
      catchError(this.handleError<any>('getDashboardBeers'))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (err: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(err) // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${err.message}`)

      // Let the app keep running by returning an empty result.
      return of(result)
    }
  }

  postCommentToBeer(id: string, comment: any): Observable<any> {
    const url = `${this.beersUrl}/${id}/comment`
    const token = localStorage.getItem('token')
    // this.httpOptions.headers.append('Authorization', 'Bearer ' + token);
    console.log(this.httpOptions)
    const temp = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    }

    console.log(temp)
    console.log(url)
    return this.http.post(url, comment, temp).pipe(tap(data => console.log(data), err => console.log(err)))
  }

  deleteCommentFromBeer(bId: string, cId: string): Observable<any> {
    const url = `${this.beersUrl}/${bId}/comment/${cId}`
    const token = localStorage.getItem('token')
    const temp = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    }
    return this.http.delete(url, temp).pipe(tap(data => console.log(data), err => console.log(err)))
  }

  getBeersFromUser(userId: string): Observable<any> {
    const url = `${this.beersUrl}/user/${userId}`
    return this.http
      .get<any>(url, this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  createBeer(beerData: any): Observable<any> {
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    }
    return this.http
      .post(this.beersUrl, JSON.stringify(beerData), this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  deleteBeer(beerId: string): Observable<any> {
    const url = `${this.beersUrl}/${beerId}`
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .delete(url, this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  updateBeer(beerId: string, beerData: any): Observable<any> {
    const url = `${this.beersUrl}/${beerId}`
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .put(url, JSON.stringify(beerData), this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }
}
