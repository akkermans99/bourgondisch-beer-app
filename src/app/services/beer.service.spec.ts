import { getTestBed, TestBed } from '@angular/core/testing'

import { BeerService } from './beer.service'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

describe('BeerService', () => {
  let service: BeerService
  let injector: TestBed
  let httpMock: HttpTestingController
  const beerUrl = 'https://avans-beer-api.herokuapp.com/api/beers'
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BeerService]
    })
    injector = getTestBed()
    service = injector.get(BeerService)
    httpMock = injector.get(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('get all beers', () => {
    service.getBeers().subscribe(value => {
      alert(value)
      expect(value).toBeTruthy()
    })
    const req = httpMock.expectOne(beerUrl)
    expect(req.request.method).toBe('GET')
  })
})
