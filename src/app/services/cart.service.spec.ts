/* tslint:disable:object-literal-key-quotes */
import { getTestBed, TestBed } from '@angular/core/testing'

import { CartService } from './cart.service'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { CartLine } from '../domain/cartLine'

describe('CartService', () => {
  let service: CartService
  let injector: TestBed
  let httpMock: HttpTestingController
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CartService]
    })
    injector = getTestBed()
    service = injector.get(CartService)
    httpMock = injector.get(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should get the cart', () => {
    expect(service.getCart()).toBeTruthy()
  })

  it('should add an item to the cart', () => {
    const id = '1'
    const cartLine: CartLine = {
      beer: {
        _id: id,
        name: 'Test',
        description: 'Test',
        content: 0.33,
        alcoholPerc: 0.08,
        avgCommentRating: 4,
        brand: 'Test',
        comments: [],
        price: 1.5,
        imageUrl: 'TEst',
        specie: ['test'],
        user: 'e'
      },
      quantity: 1,
      total: 1.5
    }
    service.addToCart(cartLine, id)
    expect(service.getCart()).toEqual({ '1': cartLine })
  })

  it('should clear the cart', () => {
    service.clearCart()
    expect(service.getCart()).toEqual({})
  })
})
