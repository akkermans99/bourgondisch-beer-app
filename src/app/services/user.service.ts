import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, map, tap } from 'rxjs/operators'
import { Observable, of } from 'rxjs'
import { User } from '../domain/user'
import { error } from 'util'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userUrl = 'https://avans-beer-api.herokuapp.com/api/users'
  adminUrl = 'https://avans-beer-api.herokuapp.com/api/admins'
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  public createUser(userData) {
    console.log(userData, JSON.stringify(userData))
    const res = this.http.post(this.userUrl, JSON.stringify(userData), this.httpOptions)
    res.subscribe(
      value => {
        console.log(value)
      },
      err => {
        console.log(err)
        alert('some of your values are not filled in correct')
      }
    )
  }

  loginUser(userData) {
    const url = `${this.userUrl}/login`
    return this.http
      .post(url, JSON.stringify(userData), this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
      .subscribe({
        next: (res: any) => {
          console.log(res)
          localStorage.setItem('token', res.token)
        },
        error: (message: any) => {
          console.log(message)
        }
      })
  }

  getAllUsers(): Observable<any> {
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .get<any>(this.userUrl, this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  getAllAdmins(): Observable<any> {
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .get<any>(this.adminUrl, this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  deleteUser(username: string): Observable<any> {
    const url = `${this.userUrl}/${username}`
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .delete(url, this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  updateUser(username: string, userData: any): Observable<any> {
    const url = `${this.userUrl}/${username}`
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .put(url, JSON.stringify(userData), this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }
}
