import { TestBed, getTestBed } from '@angular/core/testing'

import { UserService } from './user.service'
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing'
import { User } from '../domain/user'
import { HttpHeaders } from '@angular/common/http'

describe('UserService', () => {
  let service: UserService
  let injector: TestBed
  let httpMock: HttpTestingController
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    })
    injector = getTestBed()
    service = injector.get(UserService)
    httpMock = injector.get(HttpTestingController)
  })

  afterEach(() => {
    httpMock.verify()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
