import { Injectable } from '@angular/core'
import { CartLine } from '../domain/cartLine'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class CartService {
  readonly currentCart: string = 'currentcart'
  orderUrl = 'https://avans-beer-api.herokuapp.com/api/orders'
  httpOptions: {}
  constructor(private http: HttpClient) {}

  addToCart(item: CartLine, id: string): void {
    let cart: {} = this.getCart()
    if (cart !== {}) {
      if (cart[id]) {
        cart[id].quantity++
      } else {
        cart[id] = item
      }
    } else {
      cart = {
        id: item
      }
    }
    this.saveCart(cart)
  }

  getCart(): any {
    const cart: {} = JSON.parse(localStorage.getItem(this.currentCart))
    if (cart) {
      return cart
    } else {
      return {}
    }
  }

  clearCart(): void {
    localStorage.removeItem(this.currentCart)
  }

  reduceQuantityInCart(id: string): any {
    const cart: {} = this.getCart()
    if (!cart[id]) {
      return cart
    } else if (cart[id].quantity === 1) {
      return this.removeFromCart(id)
    } else {
      cart[id].quantity--
      this.saveCart(cart)
      return cart
    }
  }

  removeFromCart(id: string): any {
    const cart: {} = this.getCart()
    if (cart[id]) {
      delete cart[id]
      this.saveCart(cart)
    }
    return cart
  }

  saveCart(cart: {}): void {
    localStorage.setItem(this.currentCart, JSON.stringify(cart))
  }

  createOrder(cart: {}): Observable<any> {
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http
      .post(this.orderUrl, JSON.stringify(cart), this.httpOptions)
      .pipe(tap(data => console.log(data), err => console.log(err)))
  }

  getAllOrdersFromUser(username: string): Observable<any> {
    const url = `${this.orderUrl}/user/${username}`
    const token = localStorage.getItem('token')
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    }
    return this.http.get(url, this.httpOptions).pipe(tap(data => console.log(data), err => console.log(err)))
  }
}
