import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'

import { UserTabComponent } from './user-tab.component'
import { User, UserRoles } from '../../../domain/user'
import { UserService } from '../../../services/user.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RegisterFormComponent } from '../../register/register-form.component'
import { SettingTabComponent } from '../setting-tab/setting-tab.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NotifierModule } from 'angular-notifier'

describe('UserTabComponent', () => {
  const testUser: User = {
    _id: '5de682f4fb292e00246bd285',
    username: 'lgaakker',
    password: '$2b$10$4iA9152WaNwM/esqONt3GuD0gSlF5.ow3Lk.Sr.q61.HWDMigYHVi',
    emailAddress: 'lars@akkermansjes.nl',
    birthDate: new Date(1999, 8, 29),
    postalCode: '4901 ZG',
    address: 'Muldersteeg 10',
    country: 'Nederland',
    roles: [0, 1],
    orders: [],
    hasRole(role: UserRoles): boolean {
      return true
    }
  }
  const testToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
    'eyJ1c2VyIjp7InVzZXJuYW1lIjoibGdhYWtrZXIiLCJpZCI6IjVkZTY4MmY0ZmIyOTJlMDAyNDZiZDI4NSJ9LCJpYXQiOjE1ODU4NDc5MzksImV4cCI6MTU4NTkzNDMzOX0.' +
    'FHenP_bQ8a0Fyr5zmIGPkxAL0h6hWzITF2LaFKXg4F4'

  let component: UserTabComponent
  let fixture: ComponentFixture<UserTabComponent>
  let injector: TestBed
  let userService: UserService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserTabComponent, RegisterFormComponent, UserTabComponent, SettingTabComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        NotifierModule
      ],
      providers: [UserService]
    }).compileComponents()
    localStorage.setItem('currentuser', JSON.stringify(testUser))
    localStorage.setItem('token', testToken)
    injector = getTestBed()
    userService = injector.get(UserService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTabComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
