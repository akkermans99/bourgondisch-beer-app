import { Component, OnInit } from '@angular/core'
import { UserService } from '../../../services/user.service'
import { User } from '../../../domain/user'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-user-tab',
  templateUrl: './user-tab.component.html',
  styleUrls: ['./user-tab.component.scss']
})
export class UserTabComponent implements OnInit {
  users: User[]
  adminUsers: User[]
  constructor(private userService: UserService, private modalService: NgbModal) {
    this.getUsers()
    this.getAdminUsers()
  }

  ngOnInit() {}

  getUsers(): void {
    this.userService.getAllUsers().subscribe({
      next: value => {
        this.users = value.result
      },
      error: err => console.log(err)
    })
  }

  getAdminUsers(): void {
    this.userService.getAllAdmins().subscribe({
      next: value => {
        this.adminUsers = value.result
      },
      error: err => console.log(err)
    })
  }

  open(modal) {
    this.modalService.open(modal, {
      beforeDismiss: (): boolean | Promise<boolean> => {
        this.userService.getAllUsers().subscribe({
          next: value => (this.users = value.result),
          error: err => console.log(err)
        })
        this.userService.getAllAdmins().subscribe({
          next: value => (this.adminUsers = value.result),
          error: err => console.log(err)
        })
        return true
      }
    })
  }

  deleteUser(username: string): void {
    if (confirm(`Wil je ${username} echt verwijderen ?`)) {
      this.userService.deleteUser(username).subscribe({
        next: value => {
          this.userService.getAllUsers().subscribe({
            next: u => (this.users = u.result),
            error: err => console.log(err)
          })
          this.userService.getAllAdmins().subscribe({
            next: a => (this.adminUsers = a.result),
            error: err => console.log(err)
          })
        },
        error: err => console.log(err)
      })
    }
  }
}
