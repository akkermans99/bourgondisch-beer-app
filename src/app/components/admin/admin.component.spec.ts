import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminComponent } from './admin.component'
import { UserTabComponent } from './user-tab/user-tab.component'
import { SettingTabComponent } from './setting-tab/setting-tab.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterTestingModule } from '@angular/router/testing'
import { RegisterFormComponent } from '../register/register-form.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NotifierModule } from 'angular-notifier'

describe('AdminComponent', () => {
  let component: AdminComponent
  let fixture: ComponentFixture<AdminComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminComponent, UserTabComponent, SettingTabComponent, RegisterFormComponent],
      imports: [
        NgbModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        NotifierModule
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
