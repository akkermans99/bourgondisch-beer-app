import { Component, OnInit } from '@angular/core'
import set = Reflect.set

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  isUserScreen: boolean
  isSettingScreen: boolean

  constructor() {
    this.isUserScreen = true
    this.isSettingScreen = false
  }

  ngOnInit() {}

  setUserScreenActive(userTab, settingTab) {
    this.isUserScreen = true
    this.isSettingScreen = false
    userTab.classList.add('list-group-item-primary')
    settingTab.classList.remove('list-group-item-primary')
  }

  setSettingScreenActive(userTab, settingTab) {
    this.isUserScreen = false
    this.isSettingScreen = true
    settingTab.classList.add('list-group-item-primary')
    userTab.classList.remove('list-group-item-primary')
  }
}
