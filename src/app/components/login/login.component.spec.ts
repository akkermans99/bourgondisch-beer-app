import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'

import { LoginComponent } from './login.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { UserService } from '../../services/user.service'
import { AuthService } from '../../services/auth.service'
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms'
import { Router } from '@angular/router'
import { DashboardComponent } from '../../core/dashboard/dashboard.component'
import { BeerService } from '../../services/beer.service'
import { CarouselModule } from 'ngx-owl-carousel-o'
import { User, UserRoles } from '../../domain/user'
import { NotifierModule, NotifierService } from 'angular-notifier'
import { CartService } from '../../services/cart.service'
import { not } from 'rxjs/internal-compatibility'

describe('LoginComponent', () => {
  const testUser: User = {
    _id: '5de682f4fb292e00246bd285',
    username: 'lgaakker',
    password: '$2b$10$4iA9152WaNwM/esqONt3GuD0gSlF5.ow3Lk.Sr.q61.HWDMigYHVi',
    emailAddress: 'lars@akkermansjes.nl',
    birthDate: new Date(1999, 8, 29),
    postalCode: '4901 ZG',
    address: 'Muldersteeg 10',
    country: 'Nederland',
    roles: [0, 1],
    orders: [],
    hasRole(role: UserRoles): boolean {
      return true
    }
  }
  const testToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
    'eyJ1c2VyIjp7InVzZXJuYW1lIjoibGdhYWtrZXIiLCJpZCI6IjVkZTY4MmY0ZmIyOTJlMDAyNDZiZDI4NSJ9LCJpYXQiOjE1ODU4NDc5MzksImV4cCI6MTU4NTkzNDMzOX0.' +
    'FHenP_bQ8a0Fyr5zmIGPkxAL0h6hWzITF2LaFKXg4F4'

  let component: LoginComponent
  let fixture: ComponentFixture<LoginComponent>
  let injector: TestBed
  let userService: UserService
  let authService: AuthService
  let formBuilder: FormBuilder
  let beerService: BeerService
  let notifier: NotifierService

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, DashboardComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'login', component: LoginComponent },
          { path: 'dashboard', component: DashboardComponent }
        ]),
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        CarouselModule,
        NotifierModule
      ],
      providers: [UserService, AuthService, FormBuilder, BeerService, NotifierService]
    }).compileComponents()
    injector = getTestBed()
    userService = injector.get(UserService)
    authService = injector.get(AuthService)
    formBuilder = injector.get(FormBuilder)
    beerService = injector.get(BeerService)
    notifier = injector.get(NotifierService)
    localStorage.setItem('currentuser', JSON.stringify(testUser))
    localStorage.setItem('token', testToken)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
