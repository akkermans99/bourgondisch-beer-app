import { Component, OnInit } from '@angular/core'
import { UserService } from '../../services/user.service'
import { Router } from '@angular/router'
import { AuthService } from '../../services/auth.service'
import { FormBuilder, FormControl, Validators } from '@angular/forms'
import { NotifierService } from 'angular-notifier'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm
  username = new FormControl('', [Validators.required])
  password = new FormControl('', [Validators.required, Validators.minLength(8)])
  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private notifier: NotifierService
  ) {
    this.loginForm = this.formBuilder.group({
      username: this.username,
      password: this.password
    })
  }

  ngOnInit() {}

  async onSubmit(value) {
    if (this.loginForm.valid) {
      console.log('comes here')
      this.authService.login(value)
      console.log(this.authService.isLoggedInUser.value)
      console.log('also here')
      // this.userService.loginUser(value)
      await new Promise(resolve => setTimeout(resolve, 1200))
      const res = this.authService.isLoggedIn
      console.log(res)
      if (this.authService.isLoggedInUser.value) {
        this.loginForm.reset()
        await this.router.navigate(['/dashboard'])
      } else {
        this.notifier.notify('error', 'Incorrect username or password')
      }
    } else {
      this.notifier.notify('error', 'Not every form field is valid')
    }
  }
}
