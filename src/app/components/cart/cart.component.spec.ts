import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'

import { CartComponent } from './cart.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { CartService } from '../../services/cart.service'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'

describe('CartComponent', () => {
  let component: CartComponent
  let fixture: ComponentFixture<CartComponent>
  let injector: TestBed
  let cartService: CartService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CartComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, NgbModule, FontAwesomeModule],
      providers: [CartService]
    }).compileComponents()
    injector = getTestBed()
    cartService = injector.get(CartService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CartComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
