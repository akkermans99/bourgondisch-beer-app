import { Component, OnInit } from '@angular/core'
import { CartService } from '../../services/cart.service'
import { CartLine } from '../../domain/cartLine'
import { Beer } from '../../domain/beer'
import { faCreditCard } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cart: {}
  cartArray: CartLine[] = []
  totalPrice: number
  faCreditCard = faCreditCard

  constructor(private cartService: CartService) {
    this.getCart()
  }

  ngOnInit() {}

  convertCartToArray(): void {
    this.cartArray = []
    this.totalPrice = 0
    // tslint:disable-next-line:forin
    for (const id in this.cart) {
      const line = this.cart[id]
      line.total = line.beer.price * line.quantity
      this.totalPrice += line.total
      this.cartArray.push(line)
    }
  }

  getCart(): void {
    this.cart = this.cartService.getCart()
    this.convertCartToArray()
  }

  addQuantity(id: string, b: Beer): void {
    this.cartService.addToCart(
      new CartLine({
        beer: b,
        quantity: 1
      }),
      id
    )
    this.getCart()
  }

  reduceQuantity(id: string): void {
    this.cartService.reduceQuantityInCart(id)
    this.getCart()
  }

  removeFromCart(id: string): void {
    this.cartService.removeFromCart(id)
    this.getCart()
  }

  orderCart() {
    const body = {
      orderLines: this.cartArray
    }
    this.cartService.createOrder(body).subscribe({
      next: value => {
        console.log(value)
        this.cartService.clearCart()
        this.getCart()
      },
      error: err => console.log(err)
    })
  }
}
