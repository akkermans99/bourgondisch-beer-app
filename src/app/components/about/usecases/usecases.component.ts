import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult username en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Hiermee registreert een niet bestaande gebruiker zich',
      scenario: [
        'Gebruiker vult het registratie formulier in',
        'De applicatie of de username en email uniek is',
        'Indien alles goed is, maakt applicatie nieuwe gebruiker aan',
        'De gebruiker wordt doorgeleid naar het inlog scherm waar hij / zij direct' +
          ' kan inloggen met het zo juist gecreerde username en password'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Username en email moeten nog niet zijn gebruikt',
      postcondition: 'De actor is geregistreert'
    },
    {
      id: 'UC-03',
      name: 'Bier registreren',
      description: 'Een gebruiker kan zijn eigen gebrouwen biertje op de site zetten',
      scenario: [
        'Gebruiker logt in',
        'Gebruiker gaat naar mijn Bieren',
        'Applicatie gaat laat de door de gebruiker geregistreerde bieren zien',
        'Gebruiker drukt op de Voeg toe knop',
        'Applicatie laat modal zien waar de gebruiker de specificaties kan invullen',
        'Gebruiker vult de invoer velden in',
        'Gebruiker drukt op Voeg toe in de modal',
        'Applicatie creëert nieuw biertje en laat deze direct in de lijst zien'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker moet account hebben, bier naam moet uniek zijn',
      postcondition: 'Nieuw biertje aangemaakt'
    },
    {
      id: 'UC-04',
      name: 'Bier wijzigen',
      description: 'Een gebruiker kan zijn eigen gebrouwen biertje die op de site staat bewerken',
      scenario: [
        'Gebruiker logt in',
        'Gebruiker gaat naar mijn Bieren',
        'Applicatie gaat laat de door de gebruiker geregistreerde bieren zien',
        'Gebruiker drukt op de edit knop bij het gewenste biertje',
        'Applicatie laat modal zien waar de gebruiker de specificaties kan bewerken',
        'Gebruiker wijzigd de gewenste invoerveld waarde',
        'Gebruiker drukt op Wijzig in de modal',
        'Applicatie wijzigt het biertje en laat deze direct in de lijst zien met nieuwe waardes'
      ],
      actor: 'Reguliere gebruiker',
      precondition:
        'Gebruiker moet account hebben, bier naam moet uniek zijn, bier moet zijn aangemaakt door de ingelogde gebruiker',
      postcondition: 'Bier waardes gewijzigd'
    },
    {
      id: 'UC-05',
      name: 'Comment plaatsen',
      description: 'Een gebruiker kan onder biertjes een reactie plaatsen met een rating en beschrijving',
      scenario: [
        'Gebruiker logt in',
        'Gebruiker gaat naar Bieren',
        'Applicatie gaat laat de bieren zien',
        'Gebruiker drukt op het gewenste biertje',
        'Applicatie laat detail pagina zien van het biertje',
        'Gebruiker gaat naar comments en drukt op Voeg reactie toe',
        'Applicatie laat modal zien',
        'Gebruiker geeft rating en beschrijving',
        'Gebruiker drukt op toevoegen in de modal',
        'Applicatie voeg de comment toe en laat deze direct zien in de lijst'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker moet account hebben',
      postcondition: 'Nieuwe reactie aangemaakt'
    },
    {
      id: 'UC-06',
      name: 'Comment wijzigen',
      description: 'Een gebruiker kan onder biertjes zijn reactie wijzigen',
      scenario: [
        'Gebruiker logt in',
        'Gebruiker gaat naar Bieren',
        'Applicatie gaat laat de bieren zien',
        'Gebruiker drukt op het gewenste biertje',
        'Applicatie laat detail pagina zien van het biertje',
        'Gebruiker gaat naar comments en drukt op edit bij zijn of haar reactie',
        'Applicatie laat modal zien met waardes',
        'Gebruiker wijzigd rating of beschrijving',
        'Gebruiker drukt op wijzigen in de modal',
        'Applicatie wijzigt de comment en laat deze direct zien in de lijst'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Gebruiker moet account hebben, comment hebben geplaatst',
      postcondition: 'Reactie gewijzigd'
    },
    {
      id: 'UC-07',
      name: 'Gebruikers verwijderen',
      description: 'Een admin kan admin of reguliere gebruikers verwijderen',
      scenario: [
        'Admin logt in',
        'Admin gaat naar Admin',
        'Applicatie laat Admin pagina zien met de reguliere gebruikers en admin gebruikers',
        'Admin drukt op de verwijder knop bij de gewenste gebruiker of admin',
        'Applicatie verwijdert Gebruiker of admin'
      ],
      actor: 'Admin',
      precondition: 'Admin rechten hebben',
      postcondition: 'Gebruiker of admin is verwijderd'
    },
    {
      id: 'UC-08',
      name: 'Gebruikers toevoegen',
      description: 'Een admin kan admin of reguliere gebruikers toevoegen',
      scenario: [
        'Admin logt in',
        'Admin gaat naar Admin',
        'Applicatie laat Admin pagina zien met de reguliere gebruikers en admin gebruikers',
        'Admin drukt op de Voeg toe bij gebruikers of bij admin',
        'Applicatie laat modal zien',
        'Admin vult de invoer velden in',
        'Admin drukt op toevoegen',
        'Applicatie maakt gebruiker of admin aan'
      ],
      actor: 'Admin',
      precondition: 'Admin rechten hebben',
      postcondition: 'Gebruiker of admin is toegevoegd'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
