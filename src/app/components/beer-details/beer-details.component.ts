import { Component, OnInit } from '@angular/core'
import { Beer } from '../../domain/beer'
import { BeerService } from '../../services/beer.service'
import { Location } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { customOptions } from '../../domain/owlOptions'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { Comment } from '../../domain/comment'
import { User } from '../../domain/user'
import { CommentService } from '../../services/comment.service'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { CartService } from '../../services/cart.service'
import { CartLine } from '../../domain/cartLine'
import { NotifierService } from 'angular-notifier'

@Component({
  selector: 'app-beer-details',
  templateUrl: './beer-details.component.html',
  styleUrls: ['./beer-details.component.scss'],
  styles: [
    `
      .star {
        position: relative;
        display: inline-block;
        font-size: 1.5rem;
        color: #b0c4de;
      }
      .filled {
        color: #ffc107;
      }
      .half {
        position: absolute;
        display: inline-block;
        font-size: 1.5rem;
        color: #ffc107;
        overflow: hidden;
      }
    `
  ]
})
export class BeerDetailsComponent implements OnInit {
  beerName: string
  beer: Beer = null
  relatedBeers: Beer[]
  customOptions = customOptions
  faShoppingCart = faShoppingCart
  currentRate = 5
  commentContent = ''
  currentUserId = ''
  currentCommentId = ''
  editCommentContent = ''
  editCommentRate = 0

  constructor(
    private beerService: BeerService,
    private route: ActivatedRoute,
    private location: Location,
    private modalService: NgbModal,
    private commentService: CommentService,
    private cartService: CartService,
    private notifier: NotifierService
  ) {
    console.log('Constructor')
    route.params.subscribe(val => {
      console.log(val)
      this.beerName = val.name
      this.getBeer()
    })
  }

  ngOnInit() {
    if (localStorage.getItem('currentuser')) {
      this.currentUserId = JSON.parse(localStorage.getItem('currentuser'))._id
    }
  }

  getBeer(): void {
    // const name = this.route.snapshot.paramMap.get('name');
    // console.log(name);

    this.beerService.getBeerByName(this.beerName.toString()).subscribe(b => {
      console.log(b)
      this.beer = b.result
      this.getRelatedBeers()
    })
  }

  addToCart(): void {
    this.cartService.addToCart(
      new CartLine({
        beer: this.beer,
        quantity: 1
      }),
      this.beer._id
    )
    this.notifier.notify('success', `You have added '${this.beer.name}' to the cart!`)
  }

  getRelatedBeers(): void {
    const specie: string[] = this.beer.specie
    let stringSpecie = ''
    if (specie.length >= 2) {
      stringSpecie = specie.join('__')
    } else if (specie.length > 0 && specie.length < 2) {
      stringSpecie = specie[0]
    } else {
      stringSpecie = null
    }
    this.beerService.getBeersBySpecie(stringSpecie).subscribe(beers => {
      this.relatedBeers = beers.result
    })
  }

  goBack(): void {
    this.location.back()
  }

  open(addCommentModal) {
    this.modalService.open(addCommentModal)
  }

  addComment(modal) {
    modal.close('Add comment click')
    console.log(this.currentRate, this.commentContent)
    let comment: Comment
    const user = JSON.parse(localStorage.getItem('currentuser'))
    comment = {
      content: this.commentContent,
      rating: this.currentRate,
      author: new User(user),
      _id: null,
      submitDate: new Date(new Date().getDate().toString()),
      lastUpdateDate: new Date(new Date().getDate().toString())
    }
    // this.comments.push(comment);
    console.log(this.beer)
    this.beerService.postCommentToBeer(this.beer._id, comment).subscribe({
      next: value => {
        this.beer = value.result
      },
      error: err => {
        console.log(err)
      }
    })
  }

  deleteComment(id) {
    this.beerService.deleteCommentFromBeer(this.beer._id, id).subscribe({
      next: value => {
        console.log(value)
        this.beer = value.result
      },
      error: err => console.log(err)
    })

    /*this.beerService.getBeerById(this.beer._id)
      .subscribe({
        next: value => this.beer = value.result[0],
        error: err => console.log(err)
      });*/
  }

  editComment(modal) {
    modal.close('Edit click')
    const data = { content: this.editCommentContent, rating: this.editCommentRate }
    console.log(data)
    this.commentService.editCommentById(this.currentCommentId, data).subscribe({
      next: value => {
        this.beerService.getBeerById(this.beer._id).subscribe({
          next: b => {
            this.beer = b.result[0]
          },
          error: err => console.log(err)
        })
      },
      error: err => console.log(err)
    })
  }

  edit(editCommentModal, comment) {
    this.editCommentContent = comment.content
    this.editCommentRate = comment.rating
    this.currentCommentId = comment._id
    console.log(editCommentModal)
    this.modalService.open(editCommentModal)
  }
}
