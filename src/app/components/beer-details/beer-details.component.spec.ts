import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BeerDetailsComponent } from './beer-details.component'
import { BeerService } from '../../services/beer.service'
import { ActivatedRoute, Data } from '@angular/router'
import { Location } from '@angular/common'
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { CommentService } from '../../services/comment.service'
import { CartService } from '../../services/cart.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { fn } from '@angular/compiler/src/output/output_ast'
import { NgModule } from '@angular/core'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { FormsModule } from '@angular/forms'
import { NotifierModule, NotifierService } from 'angular-notifier'

describe('BeerDetailsComponent', () => {
  let component: BeerDetailsComponent
  let fixture: ComponentFixture<BeerDetailsComponent>

  let injector: TestBed
  let beerService: BeerService
  let route: ActivatedRoute
  let location: Location
  let modalService: NgbModal
  let commentService: CommentService
  let cartService: CartService
  let notifier: NotifierService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BeerDetailsComponent],
      imports: [
        HttpClientTestingModule,
        NgbModule,
        RouterTestingModule,
        FontAwesomeModule,
        FormsModule,
        NotifierModule
      ],
      providers: [
        CartService,
        BeerService,
        CommentService,
        Location,
        NgbModal,
        {
          provide: ActivatedRoute,
          useValue: {
            params: {
              // tslint:disable-next-line:no-shadowed-variable
              subscribe: (fn: (value: Data) => void) =>
                fn({
                  name: 'Cornet'
                })
            }
          }
        },
        NotifierModule
      ]
    }).compileComponents()

    injector = getTestBed()
    beerService = injector.get(BeerService)
    route = injector.get(ActivatedRoute)
    cartService = injector.get(CartService)
    commentService = injector.get(CommentService)
    modalService = injector.get(NgbModal)
    location = injector.get(Location)
    notifier = injector.get(NotifierService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerDetailsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
