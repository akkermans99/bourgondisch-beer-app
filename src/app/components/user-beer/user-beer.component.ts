import { Component, OnInit } from '@angular/core'
import { User } from '../../domain/user'
import { AuthService } from '../../services/auth.service'
import { BeerService } from '../../services/beer.service'
import { Beer } from '../../domain/beer'
import { faBeer } from '@fortawesome/free-solid-svg-icons'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ActivatedRoute } from '@angular/router'
import { IDropdownSettings } from 'ng-multiselect-dropdown'

@Component({
  selector: 'app-user-beer',
  templateUrl: './user-beer.component.html',
  providers: [NgbModal],
  styleUrls: ['./user-beer.component.scss']
})
export class UserBeerComponent implements OnInit {
  user: User
  userPageName: string
  userBeers: Beer[]
  newBeer: Beer = new Beer({})
  editBeer: Beer = new Beer({})
  currentUsername = ''
  faBeer = faBeer
  species: string[] = []
  dropdownSettings: any = {}

  constructor(
    private authService: AuthService,
    private beerService: BeerService,
    private modalService: NgbModal,
    private route: ActivatedRoute
  ) {
    route.params.subscribe(val => {
      this.userPageName = val.username
      this.getUserBeers()
    })
  }

  ngOnInit() {
    this.authService.getCurrentUser().subscribe({
      next: value => {
        this.user = value
        // this.getUserBeers()
      },
      error: err => console.log(err)
    })

    if (JSON.parse(localStorage.getItem('currentuser')) !== null) {
      this.currentUsername = JSON.parse(localStorage.getItem('currentuser')).username
    } else {
      this.currentUsername = ''
    }

    this.species = [
      'Blond',
      'Tripel',
      'Quadruppel',
      'Bock',
      'Pilsener',
      'Amber',
      'Dubbel',
      'Enkel',
      'Lente bock',
      'Herfst bock',
      'Wit',
      'Weizen',
      'IPA',
      'Kriek',
      'Oud bruin',
      'Stout',
      'Trappisten',
      'Saison',
      'Lambiek',
      'Geuze',
      'Bruin'
    ]
    this.dropdownSettings = {
      singleSelection: false,
      itemsShowLimit: 6,
      allowSearchFilter: true
    }
  }

  getUserBeers(): void {
    this.beerService.getBeersFromUser(this.userPageName).subscribe({
      next: beers => {
        this.userBeers = beers.result
      },
      error: err => console.log(err)
    })
  }

  openModal(beerModal) {
    this.modalService.open(beerModal)
  }

  addBeer(modal) {
    modal.close()
    console.log(this.newBeer)
    this.beerService.createBeer(this.newBeer).subscribe({
      next: value => {
        this.getUserBeers()
      },
      error: err => console.log(err)
    })
  }

  openEditModal(editModal, beer) {
    this.editBeer = beer
    this.modalService.open(editModal)
  }

  editBeerFunc(modal) {
    modal.close()
    console.log(this.editBeer)
    this.beerService.updateBeer(this.editBeer._id, this.editBeer).subscribe({
      next: value => {
        this.getUserBeers()
      },
      error: err => console.log(err)
    })
  }

  deleteBeer(beer) {
    if (confirm(`Wil je echt '${beer.name}' verwijderen ?`)) {
      this.beerService.deleteBeer(beer._id).subscribe({
        next: value => {
          this.getUserBeers()
        },
        error: err => console.log(err)
      })
    }
  }
}
