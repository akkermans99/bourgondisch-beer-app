import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'

import { UserBeerComponent } from './user-beer.component'
import { AuthService } from '../../services/auth.service'
import { BeerService } from '../../services/beer.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { User, UserRoles } from '../../domain/user'
import { ActivatedRoute, Data } from '@angular/router'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { FormsModule } from '@angular/forms'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'

describe('UserBeerComponent', () => {
  const testUser: User = {
    _id: '5de682f4fb292e00246bd285',
    username: 'lgaakker',
    password: '$2b$10$4iA9152WaNwM/esqONt3GuD0gSlF5.ow3Lk.Sr.q61.HWDMigYHVi',
    emailAddress: 'lars@akkermansjes.nl',
    birthDate: new Date(1999, 8, 29),
    postalCode: '4901 ZG',
    address: 'Muldersteeg 10',
    country: 'Nederland',
    roles: [0, 1],
    orders: [],
    hasRole(role: UserRoles): boolean {
      return true
    }
  }
  const testToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
    'eyJ1c2VyIjp7InVzZXJuYW1lIjoibGdhYWtrZXIiLCJpZCI6IjVkZTY4MmY0ZmIyOTJlMDAyNDZiZDI4NSJ9LCJpYXQiOjE1ODU4NDc5MzksImV4cCI6MTU4NTkzNDMzOX0.' +
    'FHenP_bQ8a0Fyr5zmIGPkxAL0h6hWzITF2LaFKXg4F4'

  let component: UserBeerComponent
  let fixture: ComponentFixture<UserBeerComponent>
  let injector: TestBed
  let authService: AuthService
  let beerService: BeerService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserBeerComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        NgbModule,
        FontAwesomeModule,
        FormsModule,
        NgMultiSelectDropDownModule
      ],
      providers: [
        AuthService,
        BeerService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: {
              // tslint:disable-next-line:no-shadowed-variable
              subscribe: (fn: (value: Data) => void) =>
                fn({
                  username: 'lgaakker'
                })
            }
          }
        }
      ]
    }).compileComponents()
    localStorage.setItem('currentuser', JSON.stringify(testUser))
    localStorage.setItem('token', testToken)
    injector = getTestBed()
    authService = injector.get(AuthService)
    beerService = injector.get(BeerService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBeerComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
