import { Component, OnInit } from '@angular/core'
import { User } from '../../domain/user'
import { UserService } from '../../services/user.service'
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  isUserDetailScreen: boolean
  isOrderScreen: boolean
  isSettingScreen: boolean
  constructor(private userService: UserService, private authService: AuthService) {
    this.isUserDetailScreen = true
  }

  ngOnInit() {}

  setUserDetailScreenActive(uTab, oTab, sTab) {
    this.isUserDetailScreen = true
    this.isOrderScreen = false
    this.isSettingScreen = false
    uTab.classList.add('list-group-item-primary')
    oTab.classList.remove('list-group-item-primary')
    sTab.classList.remove('list-group-item-primary')
  }

  setUserOrderScreenActive(uTab, oTab, sTab) {
    this.isUserDetailScreen = false
    this.isOrderScreen = true
    this.isSettingScreen = false
    uTab.classList.remove('list-group-item-primary')
    oTab.classList.add('list-group-item-primary')
    sTab.classList.remove('list-group-item-primary')
  }

  setSettingScreenActive(uTab, oTab, sTab) {
    this.isUserDetailScreen = false
    this.isOrderScreen = false
    this.isSettingScreen = true
    uTab.classList.remove('list-group-item-primary')
    oTab.classList.remove('list-group-item-primary')
    sTab.classList.add('list-group-item-primary')
  }
}
