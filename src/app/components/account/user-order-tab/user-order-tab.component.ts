import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../../services/auth.service'
import { CartService } from '../../../services/cart.service'
import { User } from '../../../domain/user'
import { Order } from '../../../domain/order'

@Component({
  selector: 'app-user-order-tab',
  templateUrl: './user-order-tab.component.html',
  styleUrls: ['./user-order-tab.component.scss']
})
export class UserOrderTabComponent implements OnInit {
  orders: Order[]
  user: User
  constructor(private authService: AuthService, private cartService: CartService) {
    this.authService.getCurrentUser().subscribe({
      next: value => {
        this.user = value
        this.cartService.getAllOrdersFromUser(this.user.username).subscribe({
          next: o => {
            this.orders = o.result
            console.log(this.orders)
          },
          error: err => console.log(err)
        })
      },
      error: err => console.log(err)
    })
  }

  ngOnInit() {}

  getTotal(order: Order): number {
    let total = 0
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < order.orderlines.length; i++) {
      total += order.orderlines[i].total
    }
    return total
  }
}
