import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'

import { AccountComponent } from './account.component'
import { User, UserRoles } from '../../domain/user'
import { UserService } from '../../services/user.service'
import { AuthService } from '../../services/auth.service'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { UserOrderTabComponent } from './user-order-tab/user-order-tab.component'
import { UserDetailsTabComponent } from './user-details-tab/user-details-tab.component'
import { SettingTabComponent } from '../admin/setting-tab/setting-tab.component'
import { NotifierModule } from 'angular-notifier'

describe('AccountComponent', () => {
  const testUser: User = {
    _id: '5de682f4fb292e00246bd285',
    username: 'lgaakker',
    password: '$2b$10$4iA9152WaNwM/esqONt3GuD0gSlF5.ow3Lk.Sr.q61.HWDMigYHVi',
    emailAddress: 'lars@akkermansjes.nl',
    birthDate: new Date(1999, 8, 29),
    postalCode: '4901 ZG',
    address: 'Muldersteeg 10',
    country: 'Nederland',
    roles: [0, 1],
    orders: [],
    hasRole(role: UserRoles): boolean {
      return true
    }
  }
  const testToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' +
    'eyJ1c2VyIjp7InVzZXJuYW1lIjoibGdhYWtrZXIiLCJpZCI6IjVkZTY4MmY0ZmIyOTJlMDAyNDZiZDI4NSJ9LCJpYXQiOjE1ODU4NDc5MzksImV4cCI6MTU4NTkzNDMzOX0.' +
    'FHenP_bQ8a0Fyr5zmIGPkxAL0h6hWzITF2LaFKXg4F4'

  let component: AccountComponent
  let fixture: ComponentFixture<AccountComponent>
  let injector: TestBed
  let userService: UserService
  let authService: AuthService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountComponent, UserOrderTabComponent, UserDetailsTabComponent, SettingTabComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        NgbModule,
        FormsModule,
        FontAwesomeModule,
        NotifierModule
      ],
      providers: [UserService, AuthService]
    }).compileComponents()
    localStorage.setItem('currentuser', JSON.stringify(testUser))
    localStorage.setItem('token', testToken)
    injector = getTestBed()
    userService = injector.get(UserService)
    authService = injector.get(AuthService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
