import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../../services/auth.service'
import { User } from '../../../domain/user'
import { UserService } from '../../../services/user.service'

@Component({
  selector: 'app-user-details-tab',
  templateUrl: './user-details-tab.component.html',
  styleUrls: ['./user-details-tab.component.scss']
})
export class UserDetailsTabComponent implements OnInit {
  user: User
  constructor(private authService: AuthService, private userService: UserService) {
    this.authService.getCurrentUser().subscribe({
      next: value => {
        this.user = value
      },
      error: err => console.log(err)
    })
  }

  ngOnInit() {}

  edit() {
    this.userService.updateUser(this.user.username, this.user).subscribe({
      next: value => (this.user = new User(value)),
      error: err => console.log(err)
    })
  }
}
