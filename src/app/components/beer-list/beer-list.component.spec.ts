import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing'
import { Beer } from '../../domain/beer'
import { BeerService } from '../../services/beer.service'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { CartService } from '../../services/cart.service'
import { CartLine } from '../../domain/cartLine'
import { BeerListComponent } from './beer-list.component'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { NotifierModule, NotifierService } from 'angular-notifier'

describe('BeerListComponent', () => {
  let component: BeerListComponent
  let fixture: ComponentFixture<BeerListComponent>
  let injector: TestBed
  let cartService: CartService
  let beerService: BeerService
  let notifier: NotifierService

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BeerListComponent],
      imports: [HttpClientTestingModule, FontAwesomeModule, RouterTestingModule, NotifierModule],
      providers: [BeerService, CartService, NotifierService]
    }).compileComponents()

    injector = getTestBed()
    cartService = injector.get(CartService)
    beerService = injector.get(BeerService)
    notifier = injector.get(NotifierService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerListComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
