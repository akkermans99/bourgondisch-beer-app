import { Component, OnInit } from '@angular/core'
import { Beer } from '../../domain/beer'
import { BeerService } from '../../services/beer.service'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { CartService } from '../../services/cart.service'
import { CartLine } from '../../domain/cartLine'
import { NotifierService } from 'angular-notifier'

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.scss']
})
export class BeerListComponent implements OnInit {
  beers: Beer[]
  arrayOfBeers: Beer[][]
  faShoppingCart = faShoppingCart
  constructor(
    private beerService: BeerService,
    private cartService: CartService,
    private notifier: NotifierService
  ) {}

  ngOnInit() {
    this.getBeers()
  }

  getBeers(): void {
    this.beerService.getBeers().subscribe({
      next: beers => {
        console.log(beers.result[0].specie)
        this.beers = beers.result
        this.arrayOfBeers = this.getListOfThreeBeers()
      },
      error: err => console.log(err)
    })
  }

  getListOfThreeBeers(): Beer[][] {
    const array = []
    let tempArray = []
    for (let i = 1; i < this.beers.length + 1; i++) {
      tempArray.push(this.beers[i - 1])
      if (i % 3 === 0 || this.beers.length === i) {
        array.push(tempArray)
        tempArray = []
      }
    }
    return array
  }

  addToCart(b): void {
    this.notifier.notify('success', `You have added '${b.name}' to the cart!`)
    const cartLine = new CartLine({ beer: b, quantity: 1 })
    this.cartService.addToCart(cartLine, b._id)
  }
}
