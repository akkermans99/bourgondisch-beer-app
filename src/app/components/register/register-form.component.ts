import { Component, Input, OnInit } from '@angular/core'
import { FormBuilder, FormControl, Validators } from '@angular/forms'
import { UserService } from '../../services/user.service'
import { Router } from '@angular/router'
import { UserRoles } from '../../domain/user'
import { NotifierService } from 'angular-notifier'

@Component({
  selector: 'app-register-form',
  template: `
    <notifier-container></notifier-container>
    <form [formGroup]="checkOutForm" (ngSubmit)="onSubmit(checkOutForm.value)">
      <div class="form-group">
        <label>Username</label>
        <input class="form-control" type="text" id="username" formControlName="username" />
      </div>
      <div *ngIf="username.invalid && (username.dirty || username.touched)" class="alert alert-danger">
        <div *ngIf="username.errors.required">
          Name is required.
        </div>
      </div>
      <div class="form-group">
        <label>Wachtwoord</label>
        <input type="password" class="form-control" id="password" formControlName="password" />
      </div>
      <div *ngIf="password.invalid && (password.dirty || password.touched)" class="alert alert-danger">
        <div *ngIf="password.errors.required">
          Password is required.
        </div>
        <div *ngIf="password.errors.minlength">
          Password must be at least 8 chars
        </div>
      </div>
      <div class="form-group">
        <label>Emailaddress</label>
        <input class="form-control" type="email" id="emailAddress" formControlName="emailAddress" />
      </div>
      <div
        *ngIf="emailAddress.invalid && (emailAddress.dirty || emailAddress.touched)"
        class="alert alert-danger"
      >
        <div *ngIf="emailAddress.errors.required">
          Email is required.
        </div>
        <div *ngIf="emailAddress.errors.email">
          Invalid email address!
        </div>
      </div>
      <div class="form-group">
        <label>Geboorte datum</label>
        <input class="form-control" type="date" id="birthDate" formControlName="birthDate" />
      </div>
      <div class="form-group">
        <label>Adres</label>
        <input class="form-control" type="text" id="address" formControlName="address" />
      </div>
      <div class="form-group">
        <label>Postcode</label>
        <input class="form-control" type="text" id="postalCode" formControlName="postalCode" />
      </div>
      <div class="form-group">
        <label>Land</label>
        <input class="form-control" type="text" id="country" formControlName="country" />
      </div>
      <div class="form-group">
        <button class="btn btn-success" type="submit">Registeren</button>
      </div>
    </form>
  `,
  styleUrls: ['./register.component.scss']
})
export class RegisterFormComponent implements OnInit {
  checkOutForm
  @Input() isAdmin: boolean | false
  @Input() onAdminScreen: boolean | false
  username = new FormControl('', [Validators.required])
  password = new FormControl('', [Validators.required, Validators.minLength(8)])
  emailAddress = new FormControl('', [Validators.required, Validators.email])

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router,
    private notifier: NotifierService
  ) {
    this.checkOutForm = this.formBuilder.group({
      username: this.username,
      emailAddress: this.emailAddress,
      password: this.password,
      birthDate: Date,
      address: '',
      postalCode: '',
      country: ''
    })
  }

  ngOnInit() {}

  onSubmit(value) {
    console.log(value)
    if (this.checkOutForm.valid) {
      value.roles = [UserRoles.Basic]
      if (this.isAdmin) {
        value.roles.push(UserRoles.Admin)
      }
      this.userService.createUser(value)
      this.checkOutForm.reset()
      if (!this.onAdminScreen) {
        this.router.navigate(['/login'])
      }
    } else {
      alert('going to notify u')
      this.notifier.notify('error', 'Not every form value is valid!')
    }
  }
}
