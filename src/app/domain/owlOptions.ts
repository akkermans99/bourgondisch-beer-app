import { OwlOptions } from 'ngx-owl-carousel-o'

export let customOptions: OwlOptions = {
  mouseDrag: true,
  pullDrag: true,
  touchDrag: true,
  dots: true,
  responsive: {
    0: {
      items: 1
    },
    400: {
      items: 1
    },
    740: {
      items: 2
    },
    940: {
      items: 2
    }
  },
  nav: false
}
