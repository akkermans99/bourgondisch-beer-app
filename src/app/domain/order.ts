import { CartLine } from './cartLine'
import { User } from './user'

export class Order {
  // tslint:disable-next-line:variable-name
  _id: any
  user: User
  submitDate: Date
  orderlines: CartLine[]
  constructor(values: {}) {
    Object.assign(this, values)
  }
}
