import { Beer } from './beer'

export class CartLine {
  beer: Beer
  quantity: number
  total: number
  constructor(values: {}) {
    Object.assign(this, values)
  }
}
