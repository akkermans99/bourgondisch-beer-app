export enum UserRoles {
  Basic = 0,
  Admin = 1
}

export class User {
  // tslint:disable-next-line:variable-name
  _id: any
  username: string
  emailAddress: string
  password: string
  birthDate: Date
  address: string
  postalCode: string
  orders: []
  country: string
  roles: number[]
  constructor(values: {}) {
    Object.assign(this, values)
  }

  public hasRole(role: UserRoles) {
    return this.roles.includes(role)
  }
}
