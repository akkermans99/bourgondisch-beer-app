import { Comment } from './comment'

export class Beer {
  // tslint:disable-next-line:variable-name
  _id: any
  name: string
  description: string
  price: number
  alcoholPerc: number
  content: number
  brand: string
  imageUrl: string
  comments: Comment[]
  specie: string[]
  avgCommentRating: number
  user: string
  constructor(values: {}) {
    Object.assign(this, values)
  }
}
