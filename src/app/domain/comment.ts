import { User } from './user'

export class Comment {
  content: string
  author: User
  rating: number
  // tslint:disable-next-line:variable-name
  _id: any
  submitDate: Date
  lastUpdateDate: Date
  constructor(values: {}) {
    Object.assign(this, values)
  }
}
