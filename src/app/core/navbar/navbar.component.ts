import { Component, Input, OnInit } from '@angular/core'
import { faUserCircle } from '@fortawesome/free-solid-svg-icons'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { AuthService } from '../../services/auth.service'
import { Observable, Subscription } from 'rxjs'
import { debounceTime, distinctUntilChanged, map, filter } from 'rxjs/operators'
import { Beer } from '../../domain/beer'
import { BeerService } from '../../services/beer.service'
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
    '.btn-link { color: rgba(255,255,255,.5); text-decoration: none; }',
    // tslint:disable-next-line: max-line-length
    '.btn-link.focus, .btn-link:focus, .btn-link.hover, .btn-link:hover { color: rgba(255,255,255,.75); text-decoration: none; box-shadow: none; }'
  ]
})
export class NavbarComponent implements OnInit {
  @Input() title: string
  isNavbarCollapsed = true
  faUserCircle = faUserCircle
  faShoppingCart = faShoppingCart
  isLoggedIn$: Observable<boolean>
  isAdmin$: Observable<boolean>
  usernameSubscription: Subscription
  username = ''
  beers: Beer[]
  chosenBeer: Beer

  constructor(
    private authService: AuthService,
    private beerService: BeerService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn
    this.isAdmin$ = this.authService.userIsAdmin
    this.usernameSubscription = this.authService.userFullName.subscribe(name => (this.username = name))
    this.beerService.getBeers().subscribe({
      next: beers => {
        this.beers = beers.result
      },
      error: err => console.log(err)
    })
  }

  onLogout() {
    this.authService.logout()
  }

  formatter = (beer: Beer) => beer.name

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      filter(term => term.length >= 2),
      map(term => this.beers.filter(beer => new RegExp(term, 'mi').test(beer.name)).slice(0, 10))
    )

  onSelect(input): void {
    if (this.chosenBeer) {
      this.router.navigate([`/beers/${this.chosenBeer.name}`], { relativeTo: this.route })
      input.value = ''
    }
  }
}
