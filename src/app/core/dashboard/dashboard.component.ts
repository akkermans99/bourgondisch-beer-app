import { Component, OnInit } from '@angular/core'
import { BeerService } from '../../services/beer.service'
import { Beer } from '../../domain/beer'
import { customOptions } from '../../domain/owlOptions'
import { OwlOptions } from 'ngx-owl-carousel-o'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  topRated: Beer[]
  popular: Beer[]
  customOptions: OwlOptions = customOptions

  constructor(private beerService: BeerService) {}

  ngOnInit() {
    this.beerService.getDashboardBeers().subscribe({
      next: value => {
        this.topRated = value.result.topRated
        this.popular = value.result.popular
      }
    })
  }
}
