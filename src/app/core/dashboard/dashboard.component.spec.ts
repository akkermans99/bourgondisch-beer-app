import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing'

import { DashboardComponent } from './dashboard.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { CarouselModule } from 'ngx-owl-carousel-o'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule } from '@angular/forms'
import { BeerService } from '../../services/beer.service'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'

describe('DashboardComponent', () => {
  let component: DashboardComponent
  let fixture: ComponentFixture<DashboardComponent>
  let injector: TestBed
  let beerService: BeerService
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [
        NgbModule,
        CarouselModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        FontAwesomeModule
      ],
      providers: [BeerService]
    }).compileComponents()
    injector = getTestBed()
    beerService = injector.get(BeerService)
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
