import { TestBed, async } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { UsecasesComponent } from '../../components/about/usecases/usecases.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { UsecaseComponent } from 'src/app/components/about/usecases/usecase/usecase.component'
import { DashboardComponent } from '../dashboard/dashboard.component'
import { NavbarComponent } from '../navbar/navbar.component'
import { CarouselModule } from 'ngx-owl-carousel-o'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { FormsModule } from '@angular/forms'

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgbModule,
        CarouselModule,
        HttpClientTestingModule,
        FontAwesomeModule,
        FormsModule
      ],
      declarations: [AppComponent, DashboardComponent, NavbarComponent, UsecasesComponent, UsecaseComponent]
    }).compileComponents()
  }))

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  })

  it(`should have as title 'Bourgondische bieren'`, () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app.title).toEqual('Bourgondische bieren')
  })
})
