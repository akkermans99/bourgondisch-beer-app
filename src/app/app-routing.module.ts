import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './components/about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { BeerListComponent } from './components/beer-list/beer-list.component'
import { BeerDetailsComponent } from './components/beer-details/beer-details.component'
import { RegisterComponent } from './components/register/register.component'
import { LoginComponent } from './components/login/login.component'
import { AccountComponent } from './components/account/account.component'
import { UserBeerComponent } from './components/user-beer/user-beer.component'
import { CartComponent } from './components/cart/cart.component'
import { AdminComponent } from './components/admin/admin.component'
import { AdminGuard } from './guards/admin.guard'

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'beers', component: BeerListComponent },
  { path: 'beers/:name', component: BeerDetailsComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'account', component: AccountComponent },
  { path: 'beers/user/:username', component: UserBeerComponent },
  { path: 'cart', component: CartComponent },
  { path: 'admin', canActivate: [AdminGuard], component: AdminComponent },
  { path: '**', redirectTo: '/dashboard' }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
