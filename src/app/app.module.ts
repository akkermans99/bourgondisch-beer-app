import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './components/about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './components/about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { BeerListComponent } from './components/beer-list/beer-list.component'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { HttpClientModule } from '@angular/common/http'
import { BeerDetailsComponent } from './components/beer-details/beer-details.component'
import { RegisterComponent } from './components/register/register.component'
import { LoginComponent } from './components/login/login.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CarouselModule } from 'ngx-owl-carousel-o'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AccountComponent } from './components/account/account.component'
import { UserBeerComponent } from './components/user-beer/user-beer.component'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'
import { CartComponent } from './components/cart/cart.component'
import { AdminComponent } from './components/admin/admin.component'
import { UserTabComponent } from './components/admin/user-tab/user-tab.component'
import { SettingTabComponent } from './components/admin/setting-tab/setting-tab.component'
import { RegisterFormComponent } from './components/register/register-form.component'
import { UserDetailsTabComponent } from './components/account/user-details-tab/user-details-tab.component'
import { UserOrderTabComponent } from './components/account/user-order-tab/user-order-tab.component'
import { NotifierModule, NotifierOptions } from 'angular-notifier'

/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 2500,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    BeerListComponent,
    BeerDetailsComponent,
    RegisterComponent,
    LoginComponent,
    AccountComponent,
    UserBeerComponent,
    CartComponent,
    AdminComponent,
    UserTabComponent,
    SettingTabComponent,
    RegisterFormComponent,
    UserDetailsTabComponent,
    UserOrderTabComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CarouselModule,
    ReactiveFormsModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
